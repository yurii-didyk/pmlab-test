﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PMLAB_fwdays_test
{
    class Program
    {
        private static string path = "file.txt";
        private static int messCount = 10;

        static void Main(string[] args)
        {
            var text = File.ReadAllText(path);
            Console.WriteLine(GetMagicPhrase(text));
        }

        private static string GetMagicPhrase(string text)
        {
            var charsCount = new Dictionary<char, int>();

            text.ToCharArray().Distinct().ToList()
                .ForEach(uniqueChar => charsCount.Add(uniqueChar, text.Count(ch => ch == uniqueChar)));

            var charsToRemove = charsCount.Where(x => x.Value > messCount).Select(x => x.Key).ToArray();

            return string.Join(string.Empty, text.Split(charsToRemove));
        }
    }
}
